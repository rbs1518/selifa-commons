<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Security;
use DateTime;
use DateTimeZone;
use DateInterval;
use Exception;

/**
 * Class SignedRequest
 *
 * @package RBS\Security
 * @copyright 2014-2021
 * @author Rinardi Budiadi Sarean rinardi_1518_sarean@hotmail.com
 */
class SignedRequest
{
    /**
     * @var string
     */
    protected $SignKey = '';

    /**
     * @var int
     */
    protected $ExpiryTime = 120;

    /**
     * @param int $contentlength
     * @return string
     * @throws \Exception
     */
    public static function RandomToken($contentlength = 32)
    {
        if (function_exists('random_bytes'))
        {
            $bytes = random_bytes($contentlength);
            $a = bin2hex($bytes);
        }
        else
            $a = @mcrypt_create_iv($contentlength, MCRYPT_DEV_URANDOM);
        return hash('sha256', $a, false);
    }

    /**
     * @param string $signKey
     * @param int $expiryTime
     */
    public function __construct($signKey,$expiryTime=120)
    {
        $this->SignKey = $signKey;
        $this->ExpiryTime = $expiryTime;
    }

    /**
     * Custom base64 encoding. Replace unsafe url chars
     *
     * @param string $val
     * @return string
     */
    protected function base64_url_encode($val) {

        return strtr(base64_encode($val), '+/=', '-_,');

    }

    /**
     * Custom base64 decode. Replace custom url safe values with normal
     * base64 characters before decoding.
     *
     * @param string $val
     * @return string
     */
    protected function base64_url_decode($val) {

        return base64_decode(strtr($val, '-_,', '+/='));

    }

    /**
     * @param array $data
     * @return string
     * @throws Exception
     */
    public function Encode($data)
    {
        $expireIn = $this->ExpiryTime;

        $now = new DateTime('now', new DateTimeZone('UTC'));
        $now->add(new DateInterval('PT' . $expireIn . 'S'));
        $data['Expire'] = $now->getTimestamp();

        $rawPayload = json_encode($data);
        $payload = base64_encode($rawPayload);
        $signature = hash_hmac('sha256', $payload, $this->SignKey, false);
        $encodedSignature = substr(base64_encode($signature), 0, 86);
        return ($encodedSignature[8] . $encodedSignature . $payload . '=');
    }

    /**
     * @param array $data
     * @param DateTime $expiryTime
     * @return string
     */
    public function EncodeWithSpecifiedExpiryTime($data,$expiryTime)
    {
        $data['Expire'] = $expiryTime->getTimestamp();
        $rawPayload = json_encode($data);
        $payload = base64_encode($rawPayload);
        $signature = hash_hmac('sha256', $payload, $this->SignKey, false);
        $encodedSignature = substr(base64_encode($signature), 0, 86);
        return ($encodedSignature[8] . $encodedSignature . $payload . '=');
    }

    /**
     * @param array $data
     * @return string
     * @throws Exception
     */
    public function EncodeForURL($data)
    {
        $sr = $this->Encode($data);
        return $this->base64_url_encode($sr);
    }

    /**
     * @param string $signedRequest
     * @return array
     */
    public function DecodeUnverified($signedRequest)
    {
        $aLen = (strlen($signedRequest) - 88);
        $encodedSignature = substr($signedRequest, 1, 86);
        $srSignature = base64_decode($encodedSignature . '==');
        $v['Signature'] = $srSignature;

        $payload = substr($signedRequest, 87, $aLen);
        $rawPayload = base64_decode($payload);

        //$acSignature = hash_hmac('sha256', $payload, $this->SignKey, false);
        $now = new DateTime('now', new DateTimeZone('UTC'));
        $v['CurrentTime'] = $now->getTimestamp();

        $data = json_decode($rawPayload, true);
        $v['Data'] = $data;

        return $v;
    }

    /**
     * @param string $signedRequest
     * @return array|null
     */
    public function Decode($signedRequest)
    {
        $aLen = (strlen($signedRequest) - 88);
        $encodedSignature = substr($signedRequest, 1, 86);
        $srSignature = base64_decode($encodedSignature . '==');
        $payload = substr($signedRequest, 87, $aLen);
        $rawPayload = base64_decode($payload);

        $acSignature = hash_hmac('sha256', $payload, $this->SignKey, false);
        if ($acSignature == $srSignature)
        {
            $now = new DateTime('now', new DateTimeZone('UTC'));
            $nowStamp = $now->getTimestamp();

            $data = json_decode($rawPayload, true);
            $expire = (int)$data['Expire'];
            if ($nowStamp < $expire)
            {
                return $data;
            }
            else
            {
                return null;
            }
        }
        else
        {
            return null;
        }
    }

    /**
     * @param string $encodedSignedRequest
     * @return array
     */
    public function DecodeFromURLUnverified($encodedSignedRequest)
    {
        $sr = $this->base64_url_decode($encodedSignedRequest);
        return $this->DecodeUnverified($sr);
    }

    /**
     * @param string $encodedSignedRequest
     * @return array|null
     */
    public function DecodeFromURL($encodedSignedRequest)
    {
        $sr = $this->base64_url_decode($encodedSignedRequest);
        return $this->Decode($sr);
    }
}
?>