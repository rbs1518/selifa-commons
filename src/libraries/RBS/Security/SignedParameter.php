<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Security;

/**
 * Class SignedParameter
 *
 * @package RBS\Security
 * @copyright 2014-2021
 * @author Rinardi Budiadi Sarean rinardi_1518_sarean@hotmail.com
 */
class SignedParameter
{
    /**
     * @var string
     */
    private $_Key = '';

    /**
     * @param string $key
     */
    public function __construct($key)
    {
        $this->_Key = $key;
    }

    /**
     * @param array $data
     * @return string
     */
    public function Encode($data)
    {
        $a = json_encode($data);
        $b = hash_hmac('md5',$a,$this->_Key,false);
        return substr($b,0,16).bin2hex($a).substr($b,-16);
    }

    /**
     * @param string $sParam
     * @return array|null
     */
    public function Decode($sParam)
    {
        $c = strlen($sParam);
        if ($c <= 32)
            return null;

        $a = substr($sParam,0,16).substr($sParam,-16);
        $b = hex2bin(substr($sParam,16,$c-32));
        $c = hash_hmac('md5',$b,$this->_Key,false);
        if ($a != $c)
            return null;

        return json_decode($b,true);
    }
}
?>