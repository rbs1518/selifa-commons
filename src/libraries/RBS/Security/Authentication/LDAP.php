<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Security\Authentication;
use Exception;

/**
 * Class LDAP authentication helper
 *
 * @package RBS\Security\Authentication
 * @copyright 2014
 * @author Rinardi Budiadi Sarean rinardi_1518_sarean@hotmail.com
 */
class LDAP
{
    private $_LDAPServer = '127.0.0.1';
    private $_BaseDN = '';

    /**
     * @param string $ldapServer
     * @param string $baseDN
     */
    public function __construct($ldapServer, $baseDN)
    {
        $this->_LDAPServer = $ldapServer;
        $this->_BaseDN = $baseDN;
    }

    /**
     * @param string $userName
     * @param string $password
     * @return array
     */
    public function Login($userName, $password)
    {
        $result = array('Data' => null, 'Success' => false, 'ErrorCode' => 0, 'Message' => '');
        $generalFilter = "objectClass=*";
        $searchArray = array(
            "name",
            "displayName",
            "sn",
            "givenName",
            "userPrincipalName",
            "distinguishedName",
            "sAMAccountName",

        );
		
        try
        {
            $ds = @ldap_connect($this->_LDAPServer);
            if ($ds)
            {
                $r = @ldap_bind($ds, $userName, $password);
                if ($r !== false)
                {
                    $aName = $this->GetAccountName($userName);
                    $sr = @ldap_search($ds, $this->_BaseDN, 'sAMAccountName=' . $aName, $searchArray);
                    if ($sr !== false)
                    {
                        $entries = @ldap_get_entries($ds, $sr);
                        if ($entries !== false)
                        {
                            $eCount = $entries["count"];
                            if ($eCount > 0)
                            {
                                $cred = $entries[0];
                                $finalCred = array();
                                foreach ($searchArray as $saValue)
                                {
                                    $saName = strtolower($saValue);
                                    if (isset($cred[$saName]))
                                    {
                                        $finalCred[$saValue] = $cred[$saName][0];
                                    }
                                }
                                $result['Success'] = true;
                                $result['Data'] = $finalCred;
                            }
                            else
                            {
                                $result['ErrorCode'] = 10206;
                                $result['Message'] = "LDAP entry not found. Credential does not exists.";
                            }
                        }
                        else
                        {
                            $result['ErrorCode'] = ldap_errno($ds);
                            $result['Message'] = ldap_error($ds);
                        }
                    }
                    else
                    {
                        $result['ErrorCode'] = ldap_errno($ds);
                        $result['Message'] = ldap_error($ds);
                    }
                }
                else
                {
                    $result['ErrorCode'] = ldap_errno($ds);
                    $result['Message'] = ldap_error($ds);
                }
                @ldap_close($ds);
            }
            else
            {
                $result['ErrorCode'] = ldap_errno($ds);
                $result['Message'] = ldap_error($ds);
            }
        }
        catch (Exception $x)
        {
            $result['ErrorCode'] = -1;
            $result['Message'] = ('Exception: ' . $x->getMessage());
        }

        return $result;
    }

    /**
     * @param string $text
     * @return string
     */
    protected function GetAccountName($text)
    {
        $text = trim($text);
        if (filter_var($text, FILTER_VALIDATE_EMAIL))
        {
            preg_match('/([\S\s]+?)(?=@)/', $text, $matches);
            return $matches[0];
        }
        else
        {
            preg_match("@([^\\\]*$)@", $text, $matches);
            return $matches[0];
        }
    }
}

?>