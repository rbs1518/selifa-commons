<?php
namespace RBS\Security;
use DateTime;
use Exception;

/**
 * Class WebToken
 *
 * @package RBS\Security
 * @copyright 2020-2021
 * @author Rinardi Budiadi Sarean rinardi_1518_sarean@hotmail.com
 */
class WebToken
{
    /**
     * @var string
     */
    private $_Key = '';

    /**
     * @param string $text
     * @return string
     */
    protected function Base64EncodeUrl($text)
    {
        return str_replace('=', '',strtr(base64_encode($text), '+/', '-_'));
    }

    /**
     * @param string $text
     * @return bool|string
     */
    protected function Base64DecodeUrl($text)
    {
        $remainder = strlen($text) % 4;
        if ($remainder) {
            $padlen = 4 - $remainder;
            $text .= str_repeat('=', $padlen);
        }
        return base64_decode(strtr($text, '-_', '+/'));
    }

    /**
     * @param string $key
     * @throws Exception
     */
    public function __construct($key)
    {
        $this->_Key = trim($key);
        if ($this->_Key == '')
            throw new Exception('Token key is not defined.');
    }

    /**
     * @param array $claims
     * @return string
     */
    public function Create($claims)
    {
        $head = $this->Base64EncodeUrl(json_encode(array(
            'alg' => 'HS256',
            'typ' => 'JWT'
        )));

        $defClaims = array(
            'iat' => (new DateTime())->getTimestamp()
        );

        $cti = array_replace_recursive($defClaims,$claims);
        $payload = $this->Base64EncodeUrl(json_encode($cti));

        $rawSig = ($head.'.'.$payload);

        $signature = hash_hmac('sha256',$rawSig,$this->_Key,true);
        $jwt = ($rawSig.'.'.$this->Base64EncodeUrl($signature));

        return $jwt;
    }

    /**
     * @param string|int $userId
     * @param string|int $appId
     * @param string $accessToken
     * @return string
     */
    public function CreateSimpleClaim($userId,$appId,$accessToken)
    {
        return $this->Create(array(
            'jti' => $accessToken,
            'sub' => $userId,
            'azp' => $appId
        ));
    }

    /**
     * @param string $jwt
     * @param array $requiredClaims
     * @return mixed
     * @throws Exception
     */
    public function ValidateAndDecode($jwt,$requiredClaims=array())
    {
        $jwtParts = explode('.',$jwt);
        if (count($jwtParts) != 3)
            throw new Exception('Invalid web-token.',100971);

        $head = json_decode($this->Base64DecodeUrl($jwtParts[0]),true);
        if ((!isset($head['typ'])) || (!isset($head['alg'])))
            throw new Exception('Non-standard web-token.',100972);

        $tokenType = strtolower(trim($head['typ']));
        if ($tokenType != 'jwt')
            throw new Exception('Invalid web-token type.',100973);

        $hAlgo = strtolower(trim($head['alg']));
        if ($hAlgo != 'hs256')
            throw new Exception('Unsupported algorithm.',100974);

        $payload = json_decode($this->Base64DecodeUrl($jwtParts[1]),true);
        foreach ($requiredClaims as $item)
        {
            if (!isset($payload[$item]))
                throw new Exception('One or more required claim(s) does not exists.',100975);
        }

        $rawSign = ($jwtParts[0].'.'.$jwtParts[1]);
        $tokSign = $this->Base64DecodeUrl(trim($jwtParts[2]));

        $gSign = hash_hmac('sha256',$rawSign,$this->_Key,true);
        if ($gSign !== $tokSign)
            throw new Exception('Signature mismatch.',100976);

        return $payload;
    }
}
?>