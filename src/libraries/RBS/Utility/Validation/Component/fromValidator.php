<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Utility\Validation\Component;
use RBS\Utility\Validation\Interfaces\IValidator;
use RBS\Utility\Validation\Interfaces\IValidatorAllowNull;
use RBS\Utility\Validation\Interfaces\IValidatorBoundObject;
use Exception;

/**
 * Class fromValidator
 *
 * @package RBS\Utility\Validation\Component
 */
class fromValidator implements IValidator, IValidatorBoundObject, IValidatorAllowNull
{
    /**
     * @var string
     */
    private $_PropName = '';

    /**
     * @var bool
     */
    private $_IsRequired = true;

    /**
     * @var mixed|null
     */
    private $_BObject = null;

    /**
     * @var bool
     */
    private $_IsError = false;

    /**
     * @param mixed[] $params
     * @throws Exception
     */
    public function Initialize($params)
    {
        if (isset($params[0]))
            $this->_PropName = trim($params[0]);
        else if (isset($params['key']))
            $this->_PropName = trim($params['key']);
        if ($this->_PropName == '')
            throw new Exception('Property key is empty.');

        if (isset($params[1]))
            $this->_IsRequired = (bool)$params[1];
        else if (isset($params['required']))
            $this->_IsRequired = (bool)$params['required'];
    }

    /**
     * @param mixed $value
     * @param string $key
     * @param mixed[] $values
     * @return mixed
     */
    public function Validate($value, $key, $values)
    {
        if (is_array($this->_BObject))
        {
            if (isset($this->_BObject[$this->_PropName]))
                return $this->_BObject[$this->_PropName];
            else
            {
                $this->_IsError = $this->_IsRequired;
                return null;
            }
        }
        else if (is_object($this->_BObject))
        {
            try
            {
                return $this->_BObject->{$this->_PropName};
            }
            catch (Exception $x)
            {
                $this->_IsError = $this->_IsRequired;
                return null;
            }
        }
        else
        {
            return $value;
        }
    }

    /**
     * @param string $langId
     * @return string
     */
    public function GetMessage($langId)
    {
        return "Source data does not contains '".$this->_PropName."''s value.";
    }

    /**
     * @param mixed|null $bData
     * @return mixed
     */
    public function SetBoundObject($bData)
    {
        $this->_BObject = $bData;
    }

    /**
     * @return bool
     */
    public function IsSkipNext()
    {
        return false;
    }

    /**
     * @return bool
     */
    public function IsError()
    {
        return $this->_IsError;
    }
}
?>