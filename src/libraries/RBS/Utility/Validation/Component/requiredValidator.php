<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Utility\Validation\Component;
use RBS\Utility\Validation\Interfaces\IValidator;

/**
 * Class requiredValidator
 *
 * @package RBS\Utility\Validation\Component
 */
class requiredValidator implements IValidator
{
    /**
     * @var bool
     */
    private $_AllowEmptyString = false;

    /**
     * @var bool
     */
    private $_TrimStringFirst = false;

    /**
     * @var int
     */
    private $_WhichError = 0;

    /**
     * @param mixed[] $params
     */
    public function Initialize($params)
    {
        if (isset($params['allow_empty']))
            $this->_AllowEmptyString = (bool)$params['allow_empty'];
        if (Isset($params['trim']))
            $this->_TrimStringFirst = (bool)$params['trim'];
    }

    /**
     * @param mixed $value
     * @param string $key
     * @param mixed[] $values
     * @return mixed
     */
    public function Validate($value, $key, $values)
    {
        if ($value === null)
        {
            $this->_WhichError = 1;
            return null;
        }
        else
        {
            if (is_string($value))
            {
                if ($this->_TrimStringFirst)
                    $value = trim($value);

                if ($value == '')
                {
                    if (!$this->_AllowEmptyString)
                    {
                        $this->_WhichError = 2;
                        return null;
                    }

                }
                return $value;
            }
            else
                return $value;
        }
    }

    /**
     * @param string $langId
     * @return string
     */
    public function GetMessage($langId)
    {
        if ($this->_WhichError == 1)
            return '{!key} is required but not exists.';
        else if ($this->_WhichError == 2)
            return "{!key}'s value could not be empty.";
        else
            return 'Unspecified error for {!key}.';
    }
}
?>