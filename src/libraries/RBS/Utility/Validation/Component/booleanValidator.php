<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Utility\Validation\Component;
use RBS\Utility\Validation\Interfaces\IValidator;
use Exception;

/**
 * Class booleanValidator
 *
 * Supported parameter:
 * ['true' => <true>, 'false' => <false>, 'strict' => <strict:boolean>]
 * Where
 *      <true> is the true value to match, default boolean true.
 *      <false> is the false value to match, default boolean false.
 *      <strict> (boolean, default false)
 *          To specify whether comparison is done loosely or strict to specified true/false values.
 *          If <strict> is false, then <false> won't be used. <true> value will be evaluated first.
 *          If not match then the validator will try to match '1', 1, 'Y', 'y', 'OK', or 'ok'.
 * Will output anything defined in <true> value.
 *
 * @package RBS\Utility\Validation\Component
 */
class booleanValidator implements IValidator
{
    /**
     * @var bool
     */
    private $_TrueValue = true;

    /**
     * @var bool
     */
    private $_FalseValue = false;

    /**
     * @var bool
     */
    private $_IsStrict = false;

    /**
     * @param mixed[] $params
     * @throws Exception
     */
    public function Initialize($params)
    {
        if (isset($params['true']))
            $this->_TrueValue = $params['true'];
        if (isset($params['false']))
            $this->_FalseValue = $params['false'];
        if (isset($params['strict']))
            $this->_IsStrict = (bool)$params['strict'];
    }

    /**
     * @param mixed $value
     * @param string $key
     * @param mixed[] $values
     * @return mixed
     */
    public function Validate($value, $key, $values)
    {
        if ($value === null)
            return $this->_FalseValue;

        if ($this->_IsStrict)
        {
            if ($value === $this->_TrueValue)
                return $value;
            else if ($value === $this->_FalseValue)
                return $value;
            else
                return null;
        }
        else
        {
            if ($value == $this->_TrueValue)
                return $value;
            else if ($value == '1')
                return $this->_TrueValue;
            else if ($value == 1)
                return $this->_TrueValue;
            else if (($value == 'Y') || ($value == 'y'))
                return $this->_TrueValue;
            else if (($value == 'OK') || ($value == 'ok'))
                return $this->_TrueValue;
            else
                return $this->_FalseValue;
        }
    }

    /**
     * @param string $langId
     * @return string
     */
    public function GetMessage($langId)
    {
        return "Invalid boolean value for {!key}.";
    }
}
?>