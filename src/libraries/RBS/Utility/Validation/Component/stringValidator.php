<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Utility\Validation\Component;
use RBS\Utility\Validation\Interfaces\IValidator;

/**
 * Class stringValidator
 *
 * @package RBS\Utility\Validation\Component
 */
class stringValidator implements IValidator
{
    /**
     * @var bool
     */
    private $_doConvert = false;

    /**
     * @var int
     */
    private $_maxLength = -1;

    /**
     * @var int
     */
    private $_minLength = -1;

    /**
     * @param mixed[] $params
     */
    public function Initialize($params)
    {
        if (isset($params[0]))
            $this->_doConvert = (bool)$params[0];
        else if (isset($params['convert']))
            $this->_doConvert = (bool)$params['convert'];

        if (isset($params['min']))
            $this->_minLength = (int)$params['min'];
        if (isset($params['max']))
            $this->_maxLength = (int)$params['max'];
    }

    /**
     * @param mixed $value
     * @param string $key
     * @param mixed[] $values
     * @return mixed
     */
    public function Validate($value, $key, $values)
    {
        if ($this->_doConvert)
            $value = (string)$value;
        else
        {
            if (!is_string($value))
                return null;
        }

        $len = strlen($value);
        if ($this->_minLength > -1)
        {
            if ($len >= $this->_minLength)
                return $value;
            else
                return null;
        }

        if ($this->_maxLength > -1)
        {
            if ($len <= $this->_maxLength)
                return $value;
            else
                return null;
        }

        return $value;
    }

    /**
     * @param string $langId
     * @return string
     */
    public function GetMessage($langId)
    {
        return "{!key}'s value is not a string or outside specified length.";
    }
}
?>