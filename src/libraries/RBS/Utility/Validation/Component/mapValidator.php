<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Utility\Validation\Component;
use RBS\Utility\Validation\Interfaces\IValidator;
use Exception;

/**
 * Class mapValidator
 *
 * @package RBS\Utility\Validation\Component
 */
class mapValidator implements IValidator
{
    /**
     * @var array
     */
    private $_Maps = [];

    /**
     * @var mixed|null
     */
    private $_Default = null;

    /**
     * @var bool
     */
    private $_UseDefault = false;

    /**
     * @param mixed[] $params
     * @throws Exception
     */
    public function Initialize($params)
    {
        if (isset($params[0]))
            $this->_Maps = $params[0];
        else if (isset($params['map']))
            $this->_Maps = $params['map'];

        if (isset($params[1]))
        {
            $this->_Default = $params[1];
            $this->_UseDefault = true;
        }
        else if (isset($params['default']))
        {
            $this->_Default = $params['default'];
            $this->_UseDefault = true;
        }
    }

    /**
     * @param mixed $value
     * @param string $key
     * @param mixed[] $values
     * @return mixed
     */
    public function Validate($value, $key, $values)
    {
        if (isset($this->_Maps[$value]))
            return $this->_Maps[$value];
        else
        {
            if ($this->_UseDefault)
                return $this->_Default;
            else
                return null;
        }
    }

    /**
     * @param string $langId
     * @return string
     */
    public function GetMessage($langId)
    {
        return "'{!value}' does not have mapped value for {!key}.";
    }
}
?>