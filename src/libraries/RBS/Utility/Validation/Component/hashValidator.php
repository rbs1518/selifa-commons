<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Utility\Validation\Component;
use RBS\Utility\Validation\Interfaces\IValidator;
use DateTime;

/**
 * Class hashValidator
 *
 * Supported parameter:
 * [<algo>,<src:array>,<use_time:boolean>,'algo' => <algo>,'src' => <src>,'use_time' => <use_time>]
 * Where
 *      <algo> is the algorithm name for hash function.
 *      <src> is an array of variable's key as source of inputs.
 *      <use_time> is to specify whether to include current time string in front of the concatenated result.
 *
 * This validator does not validate the value, but to create a hash from current value.
 *
 * @package RBS\Utility\Validation\Component
 */
class hashValidator implements IValidator
{
    /**
     * @var string
     */
    private $_Algo = 'md5';

    /**
     * @var string[]
     */
    private $_Sources = [];

    /**
     * @var bool
     */
    private $_UseTime = false;

    /**
     * @param mixed[] $params
     */
    public function Initialize($params)
    {
        if (isset($params[0]))
            $this->_Algo = trim($params[0]);
        else if (isset($params['algo']))
            $this->_Algo = trim($params['algo']);

        if (isset($params[1]))
            $this->_Sources = $params[1];
        else if (isset($params['src']))
            $this->_Sources = $params['src'];

        if (isset($params[2]))
            $this->_UseTime = (bool)$params[2];
        else if (isset($params['use_time']))
            $this->_UseTime = (bool)$params['use_time'];
    }

    /**
     * @param mixed $value
     * @param string $key
     * @param mixed[] $values
     * @return mixed
     */
    public function Validate($value, $key, $values)
    {
        if (count($this->_Sources) > 0)
        {
            $srcs = [];
            if ($this->_UseTime)
                $srcs[] = (new DateTime())->format('YmdHis');
            foreach ($this->_Sources as $key)
            {
                if (isset($values[$key]))
                    $srcs[] = trim($values[$key]);
            }
            return hash($this->_Algo,implode('',$srcs),false);
        }
        else
            return hash($this->_Algo,$value,false);
    }

    /**
     * @param string $langId
     * @return string
     */
    public function GetMessage($langId)
    {
        return '';
    }
}
?>