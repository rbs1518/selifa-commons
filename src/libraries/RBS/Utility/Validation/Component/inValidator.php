<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Utility\Validation\Component;
use RBS\Utility\Validation\Interfaces\IValidator;
use Exception;

/**
 * Class inValidator
 *
 * Supported parameter:
 * [<range>,'range' => <range>, 'not' => <not>, 'strict' => <strict>]
 * Where
 *      <range> an array containing values to be matched.
 *      <not> (boolean, default false) set to true to invert comparison result.
 *      <strict> (boolean, default false) is to specify whether to use strict comparator or standard.
 *
 * @package RBS\Utility\Validation\Component
 */
class inValidator implements IValidator
{
    /**
     * @var array
     */
    private $_Range = [];

    /**
     * @var bool
     */
    private $_Inverted = false;

    /**
     * @var bool
     */
    private $_IsStrict = false;

    /**
     * @param mixed[] $params
     * @throws Exception
     */
    public function Initialize($params)
    {
        if (isset($params[0]))
            $this->_Range = $params[0];
        else if (isset($params['range']))
            $this->_Range = $params['range'];

        if (isset($params['not']))
            $this->_Inverted = (bool)$params['not'];
        if (isset($params['strict']))
            $this->_IsStrict = (bool)$params['strict'];
    }

    /**
     * @param mixed $value
     * @param string $key
     * @param mixed[] $values
     * @return mixed
     */
    public function Validate($value, $key, $values)
    {
        $bIn = in_array($value,$this->_Range,$this->_IsStrict);
        if ($this->_Inverted)
            $bIn = !$bIn;

        if ($bIn)
            return $value;
        else
            return null;
    }

    /**
     * @param string $langId
     * @return string
     */
    public function GetMessage($langId)
    {
        if (!$this->_Inverted)
            return "{!key}'s value does not exists in specified range.";
        else
            return "{!key}'s value must not exists in specified range.";
    }
}
?>