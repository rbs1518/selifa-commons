<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Utility\Validation\Component;
use RBS\Utility\Validation\Interfaces\IValidator;
use Exception;

/**
 * Class betweenValidator
 *
 * Supported parameter:
 * [<lower>,<upper>] or ['lower' => <lower>, 'upper' => <upper>]
 * Where
 *      <lower> is lower boundary value (integer, float, or double)
 *      <upper> is upper boundary value (integer, float, or double)
 *      and both values are inclusive.
 *
 * @package RBS\Utility\Validation\Component
 */
class betweenValidator implements IValidator
{
    /**
     * @var null|integer|float|double
     */
    private $_LB = null;

    /**
     * @var null|integer|float|double
     */
    private $_UB = null;

    /**
     * @param mixed[] $params
     * @throws Exception
     */
    public function Initialize($params)
    {
        if (isset($params[0]))
            $this->_LB = $params[0];
        else if (isset($params['lower']))
            $this->_LB = $params['lower'];
        else
            throw new Exception('Unspecified lower bound for betweenValidator.');

        if (isset($params[1]))
            $this->_UB = $params[1];
        else if (isset($params['upper']))
            $this->_UB = $params['upper'];
        else
            throw new Exception('Unspecified upper bound for betweenValidator.');
    }

    /**
     * @param mixed $value
     * @param string $key
     * @param mixed[] $values
     * @return mixed
     */
    public function Validate($value, $key, $values)
    {
        if (($value >= $this->_LB) && ($value <= $this->_UB))
            return $value;
        else
            return null;
    }

    /**
     * @param string $langId
     * @return string
     */
    public function GetMessage($langId)
    {
        return "{!key}'s value is not between ".$this->_LB." and ".$this->_UB.".";
    }
}
?>