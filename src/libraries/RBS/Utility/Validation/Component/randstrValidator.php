<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Utility\Validation\Component;
use RBS\Utility\Validation\Interfaces\IValidator;

/**
 * Class randstrValidator
 *
 * Supported parameter:
 * [<chars>,<length>,'chars' => <algo>,'length' => <length>]
 * Where
 *      <chars> is the available characters to randomize.
 *      <length> is the length of generated string.
 *
 * This validator does not validate the value, but to create a random string.
 *
 * @package RBS\Utility\Validation\Component
 */
class randstrValidator implements IValidator
{
    /**
     * @var string
     */
    private $_Chars = 'abcdefghijklmnopqrstuvwxyz';

    /**
     * @var int
     */
    private $_Length = 4;

    /**
     * @param mixed[] $params
     */
    public function Initialize($params)
    {
        if (isset($params[0]))
            $this->_Chars = trim($params[0]);
        else if (isset($params['chars']))
            $this->_Chars = trim($params['chars']);

        if (isset($params[1]))
            $this->_Length = (int)$params[1];
        else if (isset($params['length']))
            $this->_Length = (int)$params['length'];
    }

    /**
     * @param mixed $value
     * @param string $key
     * @param mixed[] $values
     * @return mixed
     */
    public function Validate($value, $key, $values)
    {
        $cLen = strlen($this->_Chars);

        $result = '';
        for ($i=0;$i<$this->_Length;$i++)
            $result .= $this->_Chars[rand(0,$cLen-1)];

        return $result;
    }

    /**
     * @param string $langId
     * @return string
     */
    public function GetMessage($langId)
    {
        return '';
    }
}

?>