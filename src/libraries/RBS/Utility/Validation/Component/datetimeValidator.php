<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Utility\Validation\Component;
use RBS\Utility\Validation\Interfaces\IValidator;
use Exception;
use DateTime;

/**
 * Class datetimeValidator
 *
 * Supported parameter:
 * [<format>, 'format' => <format>, 'to_obj' => <to_obj:boolean>, 'after' => <after>, 'before' => <before>, 'convert' => <convert:boolean>]
 * Where
 *      <format> is specified datetime format in php notion (default to Y-m-d H:i:s).
 *      <to_obj> to specify whether to output validated value as DateTime object or as is (default false).
 *      <after> to specify minimum valid datetime that will be accepted (default null).
 *          accepts datetime formated string, DateTime object, unix timestamp, or string 'now' for current time.
 *      <before> to specify maximum valid datetime that will be accepted (default null).
 *          accepts datetime formated string, DateTime object, unix timestamp, or string 'now' for current time.
 *      <convert> to specify whether try to convert from recognized datetime formatted string or not (default false).
 *
 *
 * @package RBS\Utility\Validation\Component
 */
class datetimeValidator implements IValidator
{
    /**
     * @var string
     */
    private $_Format = 'Y-m-d H:i:s';

    /**
     * @var null|DateTime
     */
    private $_After = null;

    /**
     * @var null|DateTime
     */
    private $_Before = null;

    /**
     * @var bool
     */
    private $_OutputObject = false;

    /**
     * @var bool
     */
    private $_TryConvert = false;

    /**
     * @var int
     */
    private $_WhichError = 0;

    /**
     * @param mixed[] $params
     * @throws Exception
     */
    public function Initialize($params)
    {
        if (isset($params[0]))
            $this->_Format = trim($params[0]);
        else if (isset($params['format']))
            $this->_Format = trim($params['format']);

        if (isset($params['to_obj']))
            $this->_OutputObject = (bool)$params['to_obj'];

        if (isset($params['after']))
        {
            $prmAfter = $params['after'];
            if (is_string($prmAfter))
            {
                if (strtolower(trim($prmAfter)) == 'now')
                    $this->_After = new DateTime();
                else
                    $this->_After = DateTime::createFromFormat($this->_Format,$prmAfter);
            }
            else if ($prmAfter instanceof DateTime)
                $this->_After = $prmAfter;
            else if (is_integer($prmAfter))
                $this->_After = new DateTime('@'.$prmAfter);
            else
                throw new Exception('Invalid datetime format for after parameter in datetimeValidator.');
        }

        if (isset($params['before']))
        {
            $prmBefore = $params['before'];
            if (is_string($prmBefore))
            {
                if (strtolower(trim($prmBefore)) == 'now')
                    $this->_Before = new DateTime();
                else
                    $this->_Before = DateTime::createFromFormat($this->_Format,$prmBefore);
            }
            else if ($prmBefore instanceof DateTime)
                $this->_Before = $prmBefore;
            else if (is_integer($prmBefore))
                $this->_Before = new DateTime('@'.$prmBefore);
            else
                throw new Exception('Invalid datetime format for before parameter in datetimeValidator.');
        }

        if (isset($params['convert']))
            $this->_TryConvert = (bool)$params['convert'];
    }

    /**
     * @param mixed $value
     * @param string $key
     * @param mixed[] $values
     * @return mixed
     */
    public function Validate($value, $key, $values)
    {
        if ($this->_TryConvert)
        {
            try
            {
                $check = new DateTime($value);
            }
            catch (Exception $x)
            {
                $this->_WhichError = 2;
                return null;
            }
        }
        else
        {
            $check = DateTime::createFromFormat($this->_Format,$value);
            if ($check === false)
            {
                $this->_WhichError = 1;
                return null;
            }
        }

        if ($this->_After !== null)
        {
            if ($check <= $this->_After)
            {
                $this->_WhichError = 3;
                return null;
            }
        }

        if ($this->_Before !== null)
        {
            if ($check >= $this->_Before)
            {
                $this->_WhichError = 4;
                return null;
            }
        }

        if ($this->_OutputObject)
            return $check;
        else
        {
            if ($this->_TryConvert)
                return $check->format($this->_Format);
            else
                return $value;
        }
    }

    /**
     * @param string $langId
     * @return string
     */
    public function GetMessage($langId)
    {
        if ($this->_WhichError == 1)
            return 'Invalid datetime format for {!key}.';
        else if ($this->_WhichError == 2)
            return 'Unsupported datetime format for {!key}.';
        else if ($this->_WhichError == 3)
            return "{!key}'s value is before ".$this->_After->format($this->_Format).'.';
        else if ($this->_WhichError == 4)
            return "{!key}'s value is after ".$this->_Before->format($this->_Format).'.';
        else
            return 'Unspecified error for {!key} validation.';
    }
}
?>