<?php
namespace RBS\Utility\Validation\Component;
use RBS\Utility\Validation\Interfaces\IValidator;
use DateTime;

/**
 * Class concatValidator
 *
 * Supported parameter:
 * [<src:array>,<separator:string>,<use_time:boolean>,'src' => <src>,'separator' => <separator>,'use_time' => <use_time>]
 * Where
 *      <src> is an array of variable's key as source of inputs.
 *      <separator> is used to specify the separator string between values. Default to empty string.
 *      <use_time> is to specify whether to include current time string in front of the concatenated result.
 *
 * This validator does not validate the value, but to create a concatenated value from the values of specified keys.
 *
 * @package RBS\Utility\Validation\Component
 */
class concatValidator implements IValidator
{
    /**
     * @var string[]
     */
    private $_Sources = [];

    /**
     * @var bool
     */
    private $_UseTime = false;

    /**
     * @var string
     */
    private $_Separator = '';

    /**
     * @param mixed[] $params
     */
    public function Initialize($params)
    {
        if (isset($params[0]))
            $this->_Sources = $params[0];
        else if (isset($params['src']))
            $this->_Sources = $params['src'];
        if (isset($params[1]))
            $this->_Separator = trim($params[1]);
        else if (isset($params['separator']))
            $this->_Separator = trim($params['separator']);
        if (isset($params[2]))
            $this->_UseTime = (bool)$params[2];
        else if (isset($params['use_time']))
            $this->_UseTime = (bool)$params['use_time'];
    }

    /**
     * @param mixed $value
     * @param string $key
     * @param mixed[] $values
     * @return mixed
     */
    public function Validate($value, $key, $values)
    {
        if (count($this->_Sources) > 0)
        {
            $srcs = [];
            if ($this->_UseTime)
                $srcs[] = (new DateTime())->format('YmdHis');
            foreach ($this->_Sources as $key)
            {
                if (isset($values[$key]))
                    $srcs[] = trim($values[$key]);
            }
            return implode($this->_Separator,$srcs);
        }
        else
            return $value;
    }

    /**
     * @param string $langId
     * @return string
     */
    public function GetMessage($langId)
    {
        return '';
    }
}
?>