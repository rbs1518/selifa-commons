<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Utility\Validation\Component;
use RBS\Utility\Validation\Interfaces\IValidator;

/**
 * Class hmacValidator
 *
 * Supported parameter:
 * [<key>,<algo>,'key' => <key>, 'algo' => <algo>]
 * Where
 *      <key> is the key used for generated hmac signature.
 *      <algo> is the algorithm name for hash function.
 *
 * This validator does not validate the value, but to create a hash from current value.
 *
 * @package RBS\Utility\Validation\Component
 */
class hmacValidator implements IValidator
{
    /**
     * @var string
     */
    private $_Algo = 'sha256';

    /**
     * @var string
     */
    private $_Key = '1234567890';

    /**
     * @param mixed[] $params
     */
    public function Initialize($params)
    {
        if (isset($params[0]))
            $this->_Key = trim($params[0]);
        else if (isset($params['key']))
            $this->_Key = trim($params['key']);

        if (isset($params[1]))
            $this->_Algo = trim($params[1]);
        else if (isset($params['algo']))
            $this->_Algo = trim($params['algo']);
    }

    /**
     * @param mixed $value
     * @param string $key
     * @param mixed[] $values
     * @return mixed
     */
    public function Validate($value, $key, $values)
    {
        return hash_hmac($this->_Algo,$value,$this->_Key,false);
    }

    /**
     * @param string $langId
     * @return string
     */
    public function GetMessage($langId)
    {
        return '';
    }
}
?>