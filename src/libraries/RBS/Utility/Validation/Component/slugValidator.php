<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Utility\Validation\Component;
use RBS\Utility\Validation\Interfaces\IValidator;
use Exception;

/**
 * Class slugValidator
 *
 * @package RBS\Utility\Validation\Component
 */
class slugValidator implements IValidator
{
    /**
     * @var string
     */
    private $_SourceKey = '';

    /**
     * @param string $word
     * @return string
     */
    protected function Slugify($word)
    {
        $nWord = strtolower(preg_replace('/[^A-Z^a-z^0-9]+/','-',
            preg_replace('/([a-zd])([A-Z])/','\1-\2',
            preg_replace('/([A-Z]+)([A-Z][a-z])/','\1-\2',$word))));
        if (substr($nWord,-1) == '-')
            return substr($nWord,0,-1);
        else
            return $nWord;
    }

    /**
     * @param mixed[] $params
     * @throws Exception
     */
    public function Initialize($params)
    {
        if (isset($params[0]))
            $this->_SourceKey = trim($params[0]);
        else if (isset($params['from']))
            $this->_SourceKey = trim($params['from']);
        else
            throw new Exception('No Source key defined for slugValidator.');
    }

    /**
     * @param mixed $value
     * @param string $key
     * @param mixed[] $values
     * @return mixed
     */
    public function Validate($value, $key, $values)
    {
        if (isset($values[$this->_SourceKey]))
        {
            $text = trim($values[$this->_SourceKey]);
            if ($text != '')
                return $this->Slugify($text);
            else
                return '';
        }
        else
            return '';
    }

    /**
     * @param string $langId
     * @return string
     */
    public function GetMessage($langId)
    {
        return '';
    }
}
?>