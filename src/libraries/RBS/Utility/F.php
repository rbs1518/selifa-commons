<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Utility;
use Exception;
use SelifaException;
use ReflectionClass;
use ReflectionFunctionAbstract;

/**
 * Common Functions Wrapper
 * Note: Planned to removed in future version of selifa/commons due to most of the functions are already
 *  implemented in more relevant class. Affected functions will be marked as obsolete.
 *
 *
 * @package RBS\Utility
 * @copyright 2008-2014
 * @author Rinardi Budiadi Sarean rinardi_1518_sarean@hotmail.com
 */
class F
{
    /**
     * Retrieve a single value from $_GET, returns default value if value is not exists.
     *
     *
     * @param string $key
     * @param mixed $defaultValue
     * @return mixed
     */
    public static function Get($key, $defaultValue = null)
    {
        $result = $defaultValue;
        if (isset($_GET[$key]))
        {
            $result = $_GET[$key];
        }
        return $result;
    }

    /**
     * Retrieve a single value from $_POST, returns default value if value is not exists.
     *
     * Deprecated.
     * Use RequestBody in selifa/miria package for more complete functionality. For data validation, use Validation class.
     *
     * @deprecated deprecated since 1.1.x.
     * @param string $key
     * @param mixed $defaultValue
     * @return mixed
     */
    public static function Post($key, $defaultValue = null)
    {
        $result = $defaultValue;
        if (isset($_POST[$key]))
        {
            $result = $_POST[$key];
        }
        return $result;
    }

    /**
     * Retrieve a single value from $_POST, returns that value if exists, or call an unified error.
     * If $zeroCheck is true, then it will check whether that value is more than zero or not.
     *
     * Deprecated.
     * Use RequestBody in selifa/miria package for more complete functionality. For data validation, use Validation class.
     *
     * @deprecated deprecated since 1.1.x.
     * @param string $key
     * @param bool $zeroCheck
     * @return mixed
     * @throws SelifaException
     */
    public static function PostValue($key, $zeroCheck = false)
    {
        if (isset($_POST[$key]))
        {
            if ($zeroCheck)
            {
                $result = (int)$_POST[$key];
                if ($result > 0)
                    return $result;
                else
                    throw new SelifaException(SELIFA_EXCEPTION_NON_ZERO_IDENTIFIER);
            }
            else
                return $_POST[$key];
        }
        else
            throw new SelifaException(SELIFA_EXCEPTION_PARAMETER_IS_MISSING);
    }

    /**
     * Retrieve a single integer identifier value from an array, returns that value if exists and more than zero, or call an unified error.
     *
     * Deprecated.
     * For data validation, use Validation class. Will be removed in near future in favor of Validation class.
     *
     * @deprecated deprecated since 1.1.x.
     * @param array $array
     * @param string $key
     * @return int
     * @throws SelifaException
     */
    public static function GetNonZeroIdentifier($array, $key)
    {
        if (isset($array[$key]))
        {
            $id = (int)$array[$key];
            if ($id > 0)
            {
                return $id;
            }
            else
                throw new SelifaException(SELIFA_EXCEPTION_NON_ZERO_IDENTIFIER, $key);
        }
        else
            throw new SelifaException(SELIFA_EXCEPTION_NON_ZERO_IDENTIFIER, $key);
    }

    /**
     * Retrieve a single value from $_GET or $_POST, returns default value if value is not exists in both arrays.
     *
     * @param string $key
     * @param mixed $defaultValue
     * @return mixed
     */
    public static function Request($key, $defaultValue = null)
    {
        if (isset($_GET[$key]))
            return $_GET[$key];
        else
        {
            if (isset($_POST[$key]))
                return $_POST[$key];
            else
                return $defaultValue;
        }
    }

    /**
     * Retrieve a single value from an array, returns default value if value is not exists.
     *
     * @param array $data
     * @param string $key
     * @param mixed $defaultValue
     * @return callback|mixed|null
     */
    public static function Extract($data, $key, $defaultValue = null)
    {
        if (isset($data[$key]))
            return $data[$key];
        else
            return $defaultValue;
    }

    /**
     * Deprecated.
     * For data validation, use Validation class. Will be removed in near future in favor of Validation class.
     *
     * @deprecated deprecated since 1.1.x.
     * @param array $data
     * @param string $key
     * @param mixed|null $default
     */
    public static function ReplaceWithDefaultIfNotExists(&$data,$key,$default=null)
    {
        if (!isset($data[$key]))
            $data[$key] = $default;
        else
        {
            if ($data[$key] == '')
                $data[$key] = $default;
        }
    }

    /**
     * Deprecated.
     * For data validation, use Validation class. Will be removed in near future in favor of Validation class.
     *
     * @deprecated deprecated since 1.1.x.
     * @param array $data
     * @param array $keys
     */
    public static function MakeExists(&$data,$keys=array())
    {
        foreach ($keys as $key => $value)
        {
            if (!isset($data[$key]))
                $data[$key] = $value;
        }
    }

    /**
     * Check whether key(s) in $checks array are exists within $data array.
     *
     * Deprecated.
     * For data validation, use Validation class. Will be removed in near future in favor of Validation class.
     *
     * @deprecated deprecated since 1.1.x.
     * @param array $data
     * @param array $checks
     * @return bool
     */
    public static function CheckData($data, $checks)
    {
        $result = true;
        foreach ($checks as $cName)
        {
            $result = ($result && isset($data[$cName]));
            if (!$result)
            {
                break;
            }
        }
        return $result;
    }

    /**
     * Check whether key(s) in $checks array are exists within $data array. If only one value doesn't exists, will call unified error.
     *
     * Deprecated.
     * For data validation, use Validation class. Will be removed in near future in favor of Validation class.
     *
     * @deprecated deprecated since 1.1.x.
     * @param array $data
     * @param array $checks
     * @throws SelifaException
     */
    public static function ValidateData($data, $checks)
    {
        foreach ($checks as $cName)
        {
            if (!isset($data[$cName]))
                throw new SelifaException(SELIFA_EXCEPTION_VALUE_IS_MISSING, $cName);
        }
    }

    /**
     * Deprecated.
     * For data validation, use Validation class. Will be removed in near future in favor of Validation class.
     *
     * @deprecated deprecated since 1.1.x.
     * @param array $data
     * @param array $specArray
     * @return bool
     * @throws Exception
     */
    public static function ValidateDataEx($data,$specArray)
    {
        foreach ($specArray as $key => $eMessage)
        {
            if (!isset($data[$key]))
                throw new Exception($eMessage,SELIFA_EXCEPTION_VALUE_IS_MISSING);
        }
        return true;
    }
    #endregion

    /**
     * @return bool
     */
    public static function StartOutputBuffer()
    {
        return ob_start();
    }

    /**
     * @return string
     */
    static function EndOutputBuffer()
    {
        $s = ob_get_contents();
        ob_end_clean();
        return $s;
    }

    /**
     * @param int $length
     * @return string
     */
    public static function RandomNumeric($length)
    {
        $characters = '0123456789';
        $randomString = '';
        for ($i = 0; $i < $length; $i++)
        {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }

    /**
     * @param array $array
     * @return bool
     */
    public static function IsAssociativeArray($array)
    {
        return (array_keys($array) !== range(0, count($array) - 1));
    }

    /**
     * Deprecated.
     * Never used. Use array_replace_recursive instead.
     *
     * @deprecated deprecated since 1.1.x.
     * @param array $target
     * @throws Exception
     */
    public static function FuseArray(&$target)
    {
        $aNum = func_num_args();
        if ($aNum > 1)
        {
            for ($i=1;$i<$aNum;$i++)
            {
                $sArray = func_get_arg($i);
                if (!is_array($sArray))
                    throw new Exception('source is not an array.');
                foreach ($sArray as $key => $val)
                    $target[$key] = $val;
            }
        }
    }

    /**
     * @param string $source
     * @param string $tagName
     * @return array
     */
    public static function ParseKeyValuePairs($source,$tagName)
    {
        $pattern = '#(?<=@'.$tagName.')([\s]+)([a-z0-9_.-]+)([\s]+)([\S ]+)#';
        $matches = null;
        preg_match_all($pattern,$source,$matches);

        $result = array();
        $keyCount = count($matches[2]);
        for ($i=0;$i<$keyCount;$i++)
        {
            if ($matches[4][$i] == 'true')
                $result[$matches[2][$i]] = true;
            else if ($matches[4][$i] == 'false')
                $result[$matches[2][$i]] = false;
            else
                $result[$matches[2][$i]] = $matches[4][$i];
        }

        return $result;
    }

    /**
     * @param object|ReflectionClass|ReflectionFunctionAbstract $reflectionObj
     * @param string $tagName
     * @return array
     * @throws Exception
     */
    public static function GetParametersFromDocComment($reflectionObj,$tagName)
    {
        if ($reflectionObj instanceof ReflectionClass)
            $docComment = $reflectionObj->getDocComment();
        else if ($reflectionObj instanceof  ReflectionFunctionAbstract)
            $docComment = $reflectionObj->getDocComment();
        else if (is_object($reflectionObj))
        {
            $refObject = new ReflectionClass($reflectionObj);
            $docComment = $refObject->getDocComment();
        }
        else
            return [];

        return self::ParseKeyValuePairs($docComment,$tagName);
    }
}
?>