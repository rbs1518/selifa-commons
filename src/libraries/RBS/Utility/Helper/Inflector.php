<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Utility\Helper;

define('INFLECTOR_CAPITAL_ALL',0);
define('INFLECTOR_CAPITAL_FIRST',1);

/**
 * Class Inflector
 *
 * Inflector contains methods that can perform a string manipulation with regards to upper/lower case and singular/plural forms of words.
 * Inspired and ported from Akelos Framework by Bermi Ferrer Martinez.
 *
 * @package RBS\Utility\Helper
 * @copyright 2021
 * @author Rinardi Budiadi Sarean rinardi_1518_sarean@hotmail.com
 */
class Inflector
{
    /**
     * Converts a singular word into its plural form.
     *
     * @param string
     * @return string
     */
    public static function Pluralize($word)
    {
        $plural = [
            '/(quiz)$/i' => '\1zes',
            '/^(ox)$/i' => '\1en',
            '/([m|l])ouse$/i' => '\1ice',
            '/(matr|vert|ind)ix|ex$/i' => '\1ices',
            '/(x|ch|ss|sh)$/i' => '\1es',
            '/([^aeiouy]|qu)ies$/i' => '\1y',
            '/([^aeiouy]|qu)y$/i' => '\1ies',
            '/(hive)$/i' => '1s',
            '/(?:([^f])fe|([lr])f)$/i' => '\1\2ves',
            '/sis$/i' => 'ses',
            '/([ti])um$/i' => '\1a',
            '/(buffal|tomat)o$/i' => '\1oes',
            '/(bu)s$/i' => '\1ses',
            '/(alias|status)/i'=> '\1es',
            '/(octop|vir)us$/i'=> '\1i',
            '/(ax|test)is$/i'=> '\1es',
            '/s$/i'=> 's',
            '/$/'=> 's'
        ];

        $uncountable = ['equipment', 'information', 'rice', 'money', 'species', 'series', 'fish', 'sheep'];
        $irregular = [
            'person' => 'people',
            'man' => 'men',
            'child' => 'children',
            'sex' => 'sexes',
            'move' => 'moves'
        ];

        $lowercased_word = strtolower($word);
        foreach ($uncountable as $_uncountable)
        {
            if(substr($lowercased_word,(-1*strlen($_uncountable))) == $_uncountable)
                return $word;
        }

        foreach ($irregular as $_plural=> $_singular)
        {
            if (preg_match('/('.$_plural.')$/i', $word, $arr))
                return preg_replace('/('.$_plural.')$/i', substr($arr[0],0,1).substr($_singular,1), $word);
        }

        foreach ($plural as $rule => $replacement)
        {
            if (preg_match($rule, $word))
                return preg_replace($rule, $replacement, $word);
        }

        return false;
    }

    /**
     * Converts a plural word into its singular form.
     *
     * @param string $word
     * @return string
     */
    public static function Singularize($word)
    {
        $singular = [
            '/(quiz)zes$/i' => '\1',
            '/(matr)ices$/i' => '\1ix',
            '/(vert|ind)ices$/i' => '\1ex',
            '/^(ox)en/i' => '\1',
            '/(alias|status)es$/i' => '\1',
            '/([octop|vir])i$/i' => '\1us',
            '/(cris|ax|test)es$/i' => '\1is',
            '/(shoe)s$/i' => '\1',
            '/(o)es$/i' => '\1',
            '/(bus)es$/i' => '\1',
            '/([m|l])ice$/i' => '\1ouse',
            '/(x|ch|ss|sh)es$/i' => '\1',
            '/(m)ovies$/i' => '\1ovie',
            '/(s)eries$/i' => '\1eries',
            '/([^aeiouy]|qu)ies$/i' => '\1y',
            '/([lr])ves$/i' => '\1f',
            '/(tive)s$/i' => '\1',
            '/(hive)s$/i' => '\1',
            '/([^f])ves$/i' => '\1fe',
            '/(^analy)ses$/i' => '\1sis',
            '/((a)naly|(b)a|(d)iagno|(p)arenthe|(p)rogno|(s)ynop|(t)he)ses$/i' => '\1\2sis',
            '/([ti])a$/i' => '\1um',
            '/(n)ews$/i' => '\1ews',
            '/s$/i' => ''
        ];

        $uncountable = ['equipment', 'information', 'rice', 'money', 'species', 'series', 'fish', 'sheep'];
        $irregular = [
            'person' => 'people',
            'man' => 'men',
            'child' => 'children',
            'sex' => 'sexes',
            'move' => 'moves'
        ];

        $lowercased_word = strtolower($word);
        foreach ($uncountable as $_uncountable)
        {
            if(substr($lowercased_word,(-1*strlen($_uncountable))) == $_uncountable)
                return $word;
        }

        foreach ($irregular as $_plural=> $_singular)
        {
            if (preg_match('/('.$_singular.')$/i', $word, $arr))
                return preg_replace('/('.$_singular.')$/i', substr($arr[0],0,1).substr($_plural,1), $word);
        }

        foreach ($singular as $rule => $replacement)
        {
            if (preg_match($rule, $word))
                return preg_replace($rule, $replacement, $word);
        }

        return $word;
    }

    /**
     * Converts a word into human readable string.
     *
     * @param string $word String to "humanize"
     * @param int $type
     * @return string
     */
    public static function Humanize($word,$type=INFLECTOR_CAPITAL_FIRST)
    {
        if ($type == INFLECTOR_CAPITAL_FIRST)
            return ucfirst(str_replace('_',' ',preg_replace('/_id$/', '',$word)));
        else if ($type == INFLECTOR_CAPITAL_ALL)
            return ucwords(str_replace('_',' ',preg_replace('/_id$/', '',$word)));
        else
            return '';
    }

    /**
     * Converts a word "into_it_s_underscored_version".
     *
     * @param string $word Word to underscore
     * @return string
     */
    public static function ToUnderscore($word)
    {
        return strtolower(preg_replace('/[^A-Z^a-z^0-9]+/','_',
                    preg_replace('/([a-zd])([A-Z])/','\1_\2',
                    preg_replace('/([A-Z]+)([A-Z][a-z])/','\1_\2',$word))));
    }

    /**
     * Converts an underscored or CamelCase word into a English sentence.
     *
     * @param string $word Word to format as tile
     * @param int $type
     * @return string
     */
    public static function Titleize($word,$type=INFLECTOR_CAPITAL_ALL)
    {
        if ($type == INFLECTOR_CAPITAL_ALL)
            return ucwords(self::Humanize(self::ToUnderscore($word)));
        else if ($type == INFLECTOR_CAPITAL_FIRST)
            return ucfirst(self::Humanize(self::ToUnderscore($word)));
        else
            return '';
    }

    /**
     * Converts given word into a CamelCased version.
     *
     * @param string $word Word to convert to camel case
     * @return string
     */
    public static function Camelize($word)
    {
        return str_replace(' ','',ucwords(preg_replace('/[^A-Z^a-z^0-9]+/',' ',$word)));
    }

    /**
     * Converts given word into a variable safe version.
     *
     * @param string $word Word to lowerCamelCase
     * @return string
     */
    public static function ToVariableFormat($word)
    {
        $word = self::Camelize($word);
        return strtolower($word[0]).substr($word,1);
    }

    /**
     * Converts a class name to its table name format according to rails naming conventions.
     *
     * @param string $className Class name for getting related table_name.
     * @return string
     */
    public static function ToTableNameFormat($className)
    {
        return self::Pluralize(self::ToUnderscore($className));
    }

    /**
     * Converts a table name to its class name format according to rails naming conventions.
     *
     * @param string $tableName Table name for getting related ClassName.
     * @return string
     */
    public static function ToClassNameFormat($tableName)
    {
        return self::Camelize(self::Singularize($tableName));
    }

    /**
     * Converts number to its ordinal English form.
     *
     * @param integer $number Number to get its ordinal value
     * @return string
     */
    public static function ToOrdinalForm($number)
    {
        if (in_array(($number % 100),range(11,13)))
            return $number.'th';
        else
        {
            switch (($number % 10))
            {
                case 1:
                    return $number.'st';
                case 2:
                    return $number.'nd';
                case 3:
                    return $number.'rd';
                default:
                    return $number.'th';
            }
        }
    }
}
?>