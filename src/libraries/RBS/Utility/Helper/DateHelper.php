<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Utility\Helper;
use DateTime;

/**
 * Class DateHelper
 *
 * @package RBS\Utility\Helper
 * @copyright 2014-2021
 * @author Rinardi Budiadi Sarean rinardi_1518_sarean@hotmail.com
 */
class DateHelper
{
    /**
     * @param string $source
     * @return string
     */
    public static function ToTimeIntervalFromSeconds($source)
    {
        $left = (int)$source;
        $hour = floor($left / 3600);
        $left = ($left % 3600);
        $minute = floor($left / 60);
        $left = ($left % 60);
        return (str_pad($hour, 2, '0', STR_PAD_LEFT) . ':' . str_pad($minute, 2, '0', STR_PAD_LEFT) . ':' . str_pad(
                $left, 2, '0', STR_PAD_LEFT));
    }

    /**
     * @param string $source
     * @return DateTime
     */
    public static function FromMySQLDateTimeToObject($source)
    {
        $src = str_replace('-', ':', $source); //change to EXIF notation
        return new DateTime($src);
    }

    /**
     * @param string $input
     * @return string
     */
    public static function BootstrapDateToMySQL($input)
    {
        $a = explode('/', $input);
        return ($a[2] . '-' . $a[0] . '-' . $a[1]);
    }
}
?>