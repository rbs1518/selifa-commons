<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\System;
use ReflectionClass;
use ReflectionProperty;
use Exception;

/**
 * Class BaseTransferableObject
 * @package RBS\System
 * @copyright 2014-2021
 * @author Rinardi Budiadi Sarean rinardi_1518_sarean@hotmail.com
 */
abstract class BaseTransferableObject implements ITransferableObject
{
    /**
     * @var null|array
     */
    private static $_DataMembers = null;

    /**
     * @param string $source
     * @return array
     */
    private static function _GetTagPairs($source)
    {
        $pattern = '#(?<=@)([a-zA-Z0-9_-]+)([\s]+)([a-z0-9_.-]*)([\s]*)#';
        $matches = null;
        preg_match_all($pattern,$source,$matches);

        $result = array();
        $keyCount = count($matches[1]);
        for ($i=0;$i<$keyCount;$i++)
        {
            $theKey = trim($matches[1][$i]);
            if (isset($matches[3][$i]))
                $result[$theKey] = trim($matches[3][$i]);
            else
                $result[] = $theKey;
        }
        return $result;
    }

    /**
     * @return array
     * @throws Exception
     */
    private static function _StaticGetTransferableMembers()
    {
        if (self::$_DataMembers == null)
        {
            $r = new ReflectionClass(get_called_class());
            $aProperties = $r->getProperties(ReflectionProperty::IS_PUBLIC);

            $result = array();
            foreach ($aProperties as $prop)
            {
                $docComment = $prop->getDocComment();
                $tags = self::_GetTagPairs($docComment);
                if (isset($tags['transferable']))
                {
                    $result[] = array(
                        'Name' => $prop->getName(),
                        'DataType' => ($tags['var'] != '' ? strtolower($tags['var']) : null)
                    );
                }
            }
            self::$_DataMembers = $result;
        }
        return self::$_DataMembers;
    }

    /**
     * @return array
     * @throws Exception
     */
    public function GetTransferableMembers()
    {
        return self::_StaticGetTransferableMembers();
    }

    /**
     * @param ITransferableObject|array $tObjectOrArray
     * @throws Exception
     */
    public function To(&$tObjectOrArray)
    {
        if ($tObjectOrArray instanceof ITransferableObject)
        {
            $tMembers = $tObjectOrArray->GetTransferableMembers();
            foreach ($tMembers as $m)
            {
                $value = $this->{$m['Name']};
                if ($m['DataType'] != 'bool')
                    $tObjectOrArray->{$m['Name']} = $value;
                else
                {
                    if (is_bool($value))
                        $tObjectOrArray->{$m['Name']} = $value;
                    else
                        $tObjectOrArray->{$m['Name']} = ($value == '1');
                }
            }
        }
        else if (is_array($tObjectOrArray))
        {
            self::_StaticGetTransferableMembers();
            foreach (self::$_DataMembers as $m)
                $tObjectOrArray[$m['Name']] = $this->{$m['Name']};
        }
    }

    /**
     * @return array
     * @throws Exception
     */
    public function ToArray()
    {
        self::_StaticGetTransferableMembers();
        $result = array();
        foreach (self::$_DataMembers as $m)
            $result[$m['Name']] = $this->{$m['Name']};
        return $result;
    }

    /**
     * @param ITransferableObject|array $tObjectOrArray
     * @throws Exception
     */
    public function From($tObjectOrArray)
    {
        self::_StaticGetTransferableMembers();
        if ($tObjectOrArray instanceof ITransferableObject)
        {
            foreach (self::$_DataMembers as $m)
            {
                $value = $tObjectOrArray->{$m['Name']};
                if ($m['DataType'] != 'bool')
                    $this->{$m['Name']} = $value;
                else
                {
                    if (is_bool($value))
                        $this->{$m['Name']} = $value;
                    else
                        $this->{$m['Name']} = ($value == '1');
                }
            }
        }
        else if (is_array($tObjectOrArray))
        {
            foreach (self::$_DataMembers as $m)
            {
                if (isset($source[$m['Name']]))
                {
                    $value = $tObjectOrArray[$m['Name']];
                    if ($m['DataType'] != 'bool')
                        $this->{$m['Name']} = $value;
                    else
                    {
                        if (is_bool($value))
                            $this->{$m['Name']} = $value;
                        else
                            $this->{$m['Name']} = ($value == '1');
                    }
                }
            }
        }
    }

    /**
     * @param ITransferableObject|array $tObjectOrArray
     * @return mixed|BaseTransferableObject
     * @throws Exception
     */
    public static function CreateFrom($tObjectOrArray)
    {
        self::_StaticGetTransferableMembers();
        $calledClass = get_called_class();
        $temp = new $calledClass();

        if ($tObjectOrArray instanceof ITransferableObject)
        {
            foreach (self::$_DataMembers as $m)
            {
                $value = $tObjectOrArray->{$m['Name']};
                if ($m['DataType'] != 'bool')
                    $temp->{$m['Name']} = $value;
                else
                {
                    if (is_bool($value))
                        $temp->{$m['Name']} = $value;
                    else
                        $temp->{$m['Name']} = ($value == '1');
                }
            }
        }
        else if (is_array($tObjectOrArray))
        {
            foreach (self::$_DataMembers as $m)
            {
                if (isset($source[$m['Name']]))
                {
                    $value = $tObjectOrArray[$m['Name']];
                    if ($m['DataType'] != 'bool')
                        $temp->{$m['Name']} = $value;
                    else
                    {
                        if (is_bool($value))
                            $temp->{$m['Name']} = $value;
                        else
                            $temp->{$m['Name']} = ($value == '1');
                    }
                }
            }
        }
        return $temp;
    }
}
?>