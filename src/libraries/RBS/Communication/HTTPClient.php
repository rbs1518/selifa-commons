<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Communication;
use Exception;

/**
 * Class HTTPClient
 *
 * @package RBS\Communication
 * @copyright 2019-2021
 * @author Rinardi Budiadi Sarean rinardi_1518_sarean@hotmail.com
 */
class HTTPClient
{
    /**
     * @var null|array
     */
    private $_Spec = null;

    /**
     * @var null|callable
     */
    public $CustomAuthHandler = null;

    /**
     * @param array $spec
     *
     * Specification Keys:
     * - Name [string]
     * - URL [string]
     * - UnixSocket [string]
     * - Auth [string enum]: empty string (default), BASIC, HEADER
     * - AuthString: Content of Authorization Header. Only used if Auth = HEADER
     * - Headers [array] Array of header string
     * - IgnoreSSL [bool, default false]
     * - NullOnException [bool, default false]
     * - SendJSON [bool, default false]
     * - ProcessResponse [bool, default true]
     * - ConnectTimeout [int, don't include for default behaviour, 0 for indefinite]
     * - Timeout [int, don't include for default behaviour]
     *
     */
    public function __construct($spec)
    {
        $this->_Spec = $spec;

        if (isset($spec['CustomAuthHandler']))
            if (is_callable($spec['CustomAuthHandler']))
                $this->CustomAuthHandler = $spec['CustomAuthHandler'];
    }

    public function Send($endpoint,$method='GET',$data=null,$ovrSpec=null)
    {
        if ($ovrSpec != null)
            $fSpc = array_replace_recursive($this->_Spec,$ovrSpec);
        else
            $fSpc = $this->_Spec;

        $this->OnSpecificationPrepared($fSpc);

        if (isset($fSpc['URL']))
            $targetUri = trim($fSpc['URL']);
        else
            $targetUri = '';

        $fEndpoint = trim($endpoint);
        if (($targetUri != '') && ($fEndpoint != ''))
        {
            if (substr($targetUri,-1,1) != '/')
                $targetUri = ($targetUri.'/');
            if (substr($fEndpoint,0,1) == '/')
                $targetUri .= substr($fEndpoint,1);
            else
                $targetUri .= $fEndpoint;
        }
        else if (($targetUri == '') && ($fEndpoint != ''))
        {
            if (substr($fEndpoint,0,1) == '/')
                $targetUri = substr($fEndpoint,1);
            else
                $targetUri = $fEndpoint;
        }

        $hMethod = strtoupper(trim($method));
        if (($hMethod == 'GET') && ($data !== null))
        {
            $qs = http_build_query($data);
            if (stristr($targetUri,'?') > -1)
                $targetUri .= ('&'.$qs);
            else
                $targetUri .= ('?'.$qs);
        }

        $headers = array();
        if (isset($fSpc['Headers']))
            $headers = $fSpc['Headers'];

        $ch = curl_init();
        if (isset($fSpc['UnixSocket']))
        {
            curl_setopt($ch,CURLOPT_UNIX_SOCKET_PATH,trim($fSpc['UnixSocket']));
            $targetUri = ('http://socket/'.$targetUri);
        }

        $authType = '';
        if (isset($fSpc['Auth']))
            $authType = strtoupper(trim($fSpc['Auth']));
        if ($authType == 'BASIC')
        {
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            $authStr = '';
            if (isset($fSpc['AuthString']))
                $authStr = trim($fSpc['AuthString']);
            curl_setopt($ch, CURLOPT_USERPWD, $authStr);
        }
        else if ($authType == 'HEADER')
        {
            $authStr = '';
            if (isset($fSpc['AuthString']))
                $authStr = trim($fSpc['AuthString']);
            $headers[] = ('Authorization: '.$authStr);
        }

        $this->OnInitializingRequest($targetUri,$data,$headers);

        curl_setopt($ch, CURLOPT_URL, $targetUri);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        if ($hMethod == 'POST')
        {
            curl_setopt($ch, CURLOPT_POST, 1);

            $isSendJSON = false;
            if (isset($fSpc['SendJSON']))
                $isSendJSON = (bool)$fSpc['SendJSON'];

            if ($isSendJSON)
            {
                $headers[] = 'Content-Type: application/json';
                $pData = json_encode($data);
            }
            else
                $pData = $this->OnPrepareData($data);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $pData);
        }
        else if ($hMethod == 'GET')
        {

        }
        else
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $hMethod);
        if ($data !== null)
        {
            $isSendJSON = false;
            if (isset($fSpc['SendJSON']))
                $isSendJSON = (bool)$fSpc['SendJSON'];

            if ($isSendJSON)
                $pData = json_encode($data);
            else
                $pData = $this->OnPrepareData($data);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $pData);
        }

        if (count($headers) > 0)
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $ignoreSSL = false;
        if (isset($fSpc['IgnoreSSL']))
            $ignoreSSL = (bool)$fSpc['IgnoreSSL'];
        if ($ignoreSSL)
        {
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        }

        if (isset($fSpc['ConnectTimeout']))
        {
            $cto = (int)$fSpc['ConnectTimeout'];
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $cto);
        }

        if (isset($fSpc['Timeout']))
        {
            $eto = (int)$fSpc['Timeout'];
            curl_setopt($ch, CURLOPT_TIMEOUT, $eto);
        }

        $respHeaders = array();
        curl_setopt($ch,CURLOPT_HEADERFUNCTION,function($curl,$rhData) use (&$respHeaders)
        {
            $len = strlen($rhData);

            $hdItem = trim($rhData);
            if ($hdItem == '')
                return $len;

            if (substr($hdItem,0,4) == 'HTTP')
                return $len;

            $hParts = explode(':',$hdItem,2);
            if (count($hParts) != 2)
                return $len;

            $key = strtolower(trim($hParts[0]));
            $value = trim($hParts[1]);
            if ($value == 'true')
                $respHeaders[$key] = true;
            else if ($value == 'false')
                $respHeaders[$key] = false;
            else
                $respHeaders[$key] = $value;

            return $len;
        });

        $this->OnConfigureTransport($ch);
        $output = curl_exec($ch);

        $eMsg = trim(curl_error($ch));
        if ($eMsg != '')
        {
            $isNullOnException = false;
            if (isset($fSpc['NullOnException']))
                $isNullOnException = (bool)$fSpc['NullOnException'];
            if ($isNullOnException)
                return null;
            else
            {
                $errorNo = curl_errno($ch);
                throw new Exception($eMsg,$errorNo);
            }
        }

        $this->OnAfterTransportFinished($ch);
        $respInfo = curl_getinfo($ch);
        curl_close($ch);

        $isProcessResponse = true;
        if (isset($fSpc['ProcessResponse']))
            $isProcessResponse = (bool)$fSpc['ProcessResponse'];
        if ($isProcessResponse)
        {
            $rcType = strtolower(trim($respInfo['content_type']));
            $ctParts = explode(';',$rcType);
            if (count($ctParts) > 1)
                $rcType = trim($ctParts[0]);

            if (($rcType == 'application/json') || ($rcType == 'text/json'))
                $output = json_decode($output,true);
        }

        return $this->OnProcessResponse($output,$respInfo,$respHeaders);
    }

    /**
     * @param string $key
     * @param null|mixed $default
     * @return mixed|null
     */
    public function GetSpecValue($key,$default=null)
    {
        if (isset($this->_Spec[$key]))
            return $this->_Spec[$key];
        else
            return $default;
    }

    /**
     * @return string
     */
    public function GetName()
    {
        if (isset($this->_Spec['Name']))
            return trim($this->_Spec['Name']);
        else
            return '';
    }

    /**
     * @return string
     */
    public function GetURL()
    {
        if (isset($this->_Spec['URL']))
            return trim($this->_Spec['URL']);
        else
            return '';
    }

    protected function OnSpecificationPrepared(&$spec) { }

    protected function OnInitializingRequest($targetUri,$data,&$headers) { }

    protected function OnPrepareData($data) { return $data; }

    protected function OnConfigureTransport($ch) { }

    protected function OnAfterTransportFinished($ch) { }

    protected function OnProcessResponse($response,$info,$headers) { return $response; }
}
?>