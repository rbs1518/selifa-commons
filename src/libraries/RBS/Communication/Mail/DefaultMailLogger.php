<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Communication\Mail;
use RBS\Selifa\XM;

/**
 * Class DefaultMailLogger
 *
 * @package RBS\Communication\Mail
 */
class DefaultMailLogger implements IMailLogger
{
    /**
     * @param array $opts
     */
    public function InitializeLogger($opts)
    {

    }

    /**
     * @param MailData $mail
     * @param bool $isError
     * @param string $eMessage
     * @return bool
     */
    public function LogMailDelivery(MailData $mail, $isError = false, $eMessage = '')
    {
        XM::DumpToErrorLog('MailDeliveryLog: Error: '.$isError.' - '.$eMessage);
        XM::DumpToErrorLog($mail);
        return true;
    }
}
?>