<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Communication\Mail;
use Exception;
use RBS\Utility\F;

define('MAIL_DTR_CONTENT_BLOCK',1);
define('MAIL_DTR_CFP_CONTENT_VAR',2);
define('MAIL_DTR_CFP_CONTENT_FINALIZE',3);

/**
 * Class DefaultTemplateResolver
 *
 * @package RBS\Communication\Notification
 */
class DefaultTemplateResolver implements ITemplateResolver
{
    /**
     * @var array
     */
    private $_Templates = [];

    /**
     * @var string
     */
    private $_BasePath = '';

    /**
     * @var string
     */
    private $_DefaultPattern = '@(?<O>{)(?<Flag>[!|/])(?<Inverse>[!])?(?<Key>[a-zA-Z0-9\._-]+)(?:\((?<Parameter>[\S\s]*?)\))?(?<E>})@';

    /**
     * @param array $array
     * @return bool
     */
    private function _IsAssociativeArray($array)
    {
        return (array_keys($array) !== range(0, count($array) - 1));
    }

    /**
     * @param array $opts
     */
    public function InitializeResolver($opts)
    {
        if (isset($opts['Templates']))
            $this->_Templates = $opts['Templates'];

        $this->_BasePath = (SELIFA_ROOT_PATH.DIRECTORY_SEPARATOR);
        if (isset($opts['BasePath']))
            $this->_BasePath .= trim($opts['BasePath']);
    }

    /**
     * @param string $templateKey
     * @return MailData
     * @throws Exception
     */
    public function GetTemplate($templateKey)
    {
        $tSpec = $this->_Templates[$templateKey];
        $tFile = ($this->_BasePath.$tSpec['Path']);
        if (!file_exists($tFile))
            throw new Exception('Specified template file does not exists.');

        $mData = new MailData();
        $mData->Key = $templateKey;
        $mData->Subject = F::Extract($tSpec,'Subject','No Subject');
        $mData->Template = file_get_contents($tFile);

        return $mData;
    }

    /**
     * @param string $template
     * @param array $vars
     * @return string
     */
    public function ProcessTemplate($template,$vars)
    {
        $tokens = $this->_Tokenize($template);
        return $this->_Render($tokens,$vars);
    }

    #region Tokenizer
    /**
     * @param string $source
     * @param string $pattern
     * @param array $parsedStruct
     * @param int $startPos
     * @return array
     */
    private function _TemplateToTokens($source,$pattern,&$parsedStruct,&$startPos)
    {
        $tResult = array('MK'=>'','LastPos'=>0,'EndPos'=>0);
        while (preg_match($pattern, $source, $matched, PREG_OFFSET_CAPTURE, $startPos) > 0)
        {
            $currentKey = trim($matched['Key'][0]);
            $params = array();
            if (isset($matched['Parameter']))
            {
                $xParamString = trim($matched['Parameter'][0]);
                if ($xParamString != '')
                {
                    $temp = explode(',',$xParamString);
                    foreach ($temp as $value)
                    {
                        if ($value[0] == "'")
                            $params[] = array('key'=>null,'value'=>substr($value,1,strlen($value)-2));
                        else
                            $params[] = array('key'=>$value,'value'=>null);
                    }
                }
            }

            $isInverted = false;
            if ($matched['Flag'][0] == '/')
            {
                $tResult = array('MK'=>$currentKey,'LastPos'=>$startPos,'EndPos'=>$matched['O'][1]);
                $startPos = ($matched['O'][1] + strlen('{!'.$matched['Key'][0].'}'));
                break;
            }
            else
            {
                if (isset($matched['Inverse']))
                    $isInverted = ($matched['Inverse'][0] == '!');
            }

            $token = array(
                't_start' => substr($source,$startPos,($matched['O'][1]-$startPos)),
                'startpos' => $matched['O'][1],
                'type' => MAIL_DTR_CONTENT_BLOCK,
                'id' => $matched['Key'][0],
                'key' => $currentKey,
                'params' => $params,
                'inverted' => $isInverted,
                'content' => '',
                't_end' => ''
            );
            $startPos = ($matched['O'][1] + strlen($matched[0][0]));

            $childs = array();
            $mRes = $this->_TemplateToTokens($source,$pattern,$childs,$startPos);
            if ($mRes['MK'] == $token['key'])
            {
                $token['t_end'] = substr($source,$mRes['LastPos'],($mRes['EndPos']-$mRes['LastPos']));
                $token['childs'] = $childs;
                $parsedStruct[] = $token;
            }
        }

        return $tResult;
    }

    /**
     * @param string $source
     * @param string $pattern
     * @param array $parsedStruct
     * @param int $startPos
     */
    private function _TemplateToTokensFinalize($source,$pattern,&$parsedStruct,&$startPos)
    {
        $token = array(
            't_start' => substr($source,$startPos),
            'startpos' => $startPos,
            'type' => MAIL_DTR_CFP_CONTENT_FINALIZE,
            'id' => '',
            'key' => '',
            'params' => array(),
            'inverted' => false,
            'content' => '',
            't_end' => '',
            'childs' => array()
        );
        $parsedStruct[] = $token;
    }

    /**
     * @param string $text
     * @return array
     */
    private function _Tokenize($text)
    {
        $startPos = 0;
        $pTokens = array(
            't_start' => '',
            'startpos' => $startPos,
            'type' => MAIL_DTR_CONTENT_BLOCK,
            'id' => '',
            'key' => '',
            'params' => array(),
            'inverted' => false,
            'content' => '',
            't_end' => '',
            'childs' => array()
        );
        $this->_TemplateToTokens($text,$this->_DefaultPattern,$pTokens['childs'],$startPos);
        $this->_TemplateToTokensFinalize($text,$this->_DefaultPattern,$pTokens['childs'],$startPos);
        return $pTokens;
    }
    #endregion

    #region Renderer
    /**
     * @param array $tokens
     * @param array $vars
     * @return string
     */
    private function _Render($tokens,$vars)
    {
        $result = '';
        foreach ($tokens['childs'] as $child)
        {
            $result .= $child['t_start'];
            if ($child['type'] == MAIL_DTR_CONTENT_BLOCK)
            {
                $tempResult = '';
                $vObject = null;
                if (isset($vars[$child['key']]))
                    $vObject = $vars[$child['key']];

                if ($vObject !== null)
                {
                    if (is_bool($vObject))
                    {
                        if ($child['inverted'])
                            $vObject = !$vObject;
                        if ($vObject)
                        {
                            $tempResult .= $this->_Render($child, $vars);
                            $tempResult .= $child['t_end'];
                        }
                    }
                    else if (is_array($vObject))
                    {
                        if (count($vObject) > 0)
                        {
                            if ($this->_IsAssociativeArray($vObject))
                            {
                                $tempResult .= $this->_Render($child, $vObject);
                                $tempResult .= $child['t_end'];
                            }
                            else
                            {
                                foreach ($vObject as $vRow)
                                {
                                    $tempResult .= $this->_Render($child, $vRow);
                                    $tempResult .= $child['t_end'];
                                }
                            }
                        }
                    }
                }
                $result .= $tempResult;
            }
            else if ($child['type'] == MAIL_DTR_CFP_CONTENT_FINALIZE)
            {

            }
        }

        return $result;
    }
    #endregion

}
?>