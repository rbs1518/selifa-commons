<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Communication\Mail;
use RBS\Selifa\IComponent;
use RBS\Selifa\Traits\OptionableSingleton;
use RBS\Utility\F;
use Exception;

/**
 * Class MailSender
 * @package RBS\Communication\Notification
 */
class MailSender implements IComponent
{
    use OptionableSingleton;

    /**
     * @var null|IMailSender
     */
    private $_Sender = null;

    /**
     * @var null|IDataResolver
     */
    private $_DataResolver = null;

    /**
     * @var null|ITemplateResolver
     */
    private $_TemplateResolver = null;

    /**
     * @var null|IMailLogger
     */
    private $_Logger = null;

    /**
     * @param array $options
     */
    protected function _OnInit($options)
    {

    }

    /**
     * @return IMailSender
     * @throws Exception
     */
    protected function CreateSender()
    {
        $cn = trim(F::Extract($this->_Options,'SenderClass',''));
        if ($cn == '')
            throw new Exception('Empty sender class.');

        $senderOpts = F::Extract($this->_Options,'SenderOpts',[]);
        $senderObj = new $cn($senderOpts);
        if (!($senderObj instanceof IMailSender))
            throw new Exception('Class ['.$cn.'] does not implement IMailSender interface.');

        return $senderObj;
    }

    /**
     * @return IDataResolver
     * @throws Exception
     */
    protected function CreateDataResolver()
    {
        $cn = trim(F::Extract($this->_Options,'DataResolverClass',''));
        if ($cn == '')
            throw new Exception('Empty data resolver class.');

        $drOpts = F::Extract($this->_Options,'DataResolverOpts',[]);
        $drObject = new $cn();
        if (!($drObject instanceof IDataResolver))
            throw new Exception('Class ['.$cn.'] does not implement IDataResolver interface.');

        $drObject->InitializeResolver($drOpts);
        return $drObject;
    }

    /**
     * @return ITemplateResolver
     * @throws Exception
     */
    protected function CreateTemplateResolver()
    {
        $cn = trim(F::Extract($this->_Options,'TemplateResolverClass',''));
        if ($cn == '')
            throw new Exception('Empty template resolver class.');

        $trOpts = F::Extract($this->_Options,'TemplateResolverOpts',[]);
        $trObject = new $cn();
        if (!($trObject instanceof ITemplateResolver))
            throw new Exception('Class ['.$cn.'] does not implement ITemplateResolver interface.');

        $trObject->InitializeResolver($trOpts);
        return $trObject;
    }

    /**
     * @return null|IMailLogger
     * @throws Exception
     */
    protected function CreateMailLogger()
    {
        $cn = trim(F::Extract($this->_Options,'LoggerClass',''));
        if ($cn == '')
            return null;

        $logOpts = F::Extract($this->_Options,'LoggerOpts',[]);
        $logObj = new $cn();
        if (!($logObj instanceof IMailLogger))
            throw new Exception('Class ['.$cn.'] does not implement IMailLogger interface.');

        $logObj->InitializeLogger($logOpts);
        return $logObj;
    }

    /**
     * @throws Exception
     */
    protected function CreateResolvers()
    {
        if ($this->_Sender == null)
            $this->_Sender = $this->CreateSender();
        if ($this->_DataResolver == null)
            $this->_DataResolver = $this->CreateDataResolver();
        if ($this->_TemplateResolver == null)
            $this->_TemplateResolver = $this->CreateTemplateResolver();
        if ($this->_Logger == null)
            $this->_Logger = $this->CreateMailLogger();
    }

    /**
     * @param IDataResolver $resolver
     * @return MailSender
     */
    public function SetDataResolver(IDataResolver $resolver)
    {
        $this->_DataResolver = $resolver;
        return $this;
    }

    /**
     * @param ITemplateResolver $resolver
     * @return MailSender
     */
    public function SetTemplateResolver(ITemplateResolver $resolver)
    {
        $this->_TemplateResolver = $resolver;
        return $this;
    }

    /**
     * @param IMailLogger $logger
     * @return MailSender
     */
    public function SetMailLogger(IMailLogger $logger)
    {
        $this->_Logger = $logger;
        return $this;
    }

    /**
     * @param string $templateKey
     * @param string|int $toUserId
     * @param array $data
     * @return bool
     * @throws Exception
     */
    public function Send($templateKey,$toUserId,$data)
    {
        $this->CreateResolvers();

        $aSenderName = trim(F::Extract($this->_Options,'SenderName',''));
        $aSenderAddr = trim(F::Extract($this->_Options,'SenderAddress',''));
        if (($aSenderName == '') || ($aSenderAddr == ''))
            throw new Exception('Mail sender identifier is incomplete.');

        $mail = $this->_TemplateResolver->GetTemplate($templateKey);

        $mailDest = $this->_DataResolver->Resolve($toUserId);
        if (is_array($mailDest->TemplateData))
            $data = array_replace_recursive($data,$mailDest->TemplateData);
        $mail->Destination = $mailDest;
        $mail->Variables = $data;

        $mail->Body = $this->_TemplateResolver->ProcessTemplate(
            $mail->Template,
            $mail->Variables);

        try
        {
            $result = $this->_Sender->SendMail([
                'name' => $aSenderName,
                'email' => $aSenderAddr
            ],$mail);

            if ($this->_Logger != null)
                $this->_Logger->LogMailDelivery($mail,false,'');
            return $result;

        }
        catch (Exception $x)
        {
            if ($this->_Logger != null)
                $this->_Logger->LogMailDelivery($mail,true,$x->getMessage());
            return false;
        }
    }
}
?>