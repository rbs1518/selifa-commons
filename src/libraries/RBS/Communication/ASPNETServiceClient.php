<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Communication;
use SoapClient;

/**
 * Class ASPNETServiceClient
 *
 * @package RBS\Communication
 * @copyright 2008-2009
 * @author Rinardi Budiadi Sarean rinardi_1518_sarean@hotmail.com
 */
class ASPNETServiceClient
{
    /**
     * @var SoapClient
     */
	public $client;

    /**
     * ASPNETServiceClient constructor.
     *
     * @param string $WSDL_URL
     * @param array $classmaps
     * @param bool $cacheWSDL
     */
	public function __construct($WSDL_URL,$classmaps=array(),$cacheWSDL=true)
	{
	    /*$WSDL_URL => Address to WebService's WSDL*/
		//ini_set('soap.wsdl_cache_ttl', 1);
		if ($cacheWSDL)
		{
			$this->client = new SoapClient($WSDL_URL,array(
			"soap_version"=>SOAP_1_2,
			"classmap"=>$classmaps));			
		}
		else
		{
			$this->client = new SoapClient($WSDL_URL,array(
				"soap_version"=>SOAP_1_2,
				"classmap"=>$classmaps,
				"cache_wsdl"=>WSDL_CACHE_NONE));
		}
	}

    /**
     * @param object|array $obj
     * @return array
     */
	public function _ObjectToArray($obj)
	{	$out = array();
	 	foreach ($obj as $key => $val)
		{	switch (true)
			{	case is_object($val):
					$out[$key] = $this->_ObjectToArray($val);
					break;
				case is_array($val):
					$out[$key] = $this->_ObjectToArray($val);
					break;
				default:
					$out[$key] = $val;
	 		}
		}
	 	return $out;
	}

    /**
     * @param array $array
     * @return bool
     */
    public function IsAssocArray($array)
    {
        if (is_array($array) && !empty($array))
        {
            for ($iterator = count($array) - 1; $iterator; $iterator--)
            {
                if (!array_key_exists($iterator, $array))
                {
                    return true;
                }
            }
            return !array_key_exists(0, $array);
        }
        return false;
    }

    /**
     * @param string $name
     * @param array $params
     * @return mixed|null
     */
	public function __call($name,$params)
	{
        if ( $name[0] != '_' )
		{	if ( count($params) > 0 )
				$p = $params;
			else $p = array();			
			$result = $this->client->__soapCall($name,$p);
			if ($result !== NULL)
			{	$result = $this->_ObjectToArray($result);
				if ( array_key_exists($name.'Result',$result) )
				{	$res = $result[$name.'Result'];
					if ( is_array($res) == true )
					{	if ( count($res) > 1 )
							return $res;
						else if ( count($res) > 0 )
						{	$keys = array_keys($res);
							return $res[$keys[0]];
						} else return array();

						//return $res;
					} else return $res;
				} else return NULL;
			} else return NULL;
		}

        return null;
	}
}
?>