<?php
return [
    'configs' => [
        'rbs_communication_mail_mailsender' => [
            'SenderName' => 'Selifa Mail Sender',
            'SenderAddress' => 'sender@dummy.com',

            'SenderClass' => "RBS\\Communication\\Mail\\DummySender",
            'SenderOpts' => [],

            'DataResolverClass' => "RBS\\Communication\\Mail\\DefaultDataResolver",
            'DataResolverOpts' => [],

            'TemplateResolverClass' => "RBS\\Communication\\Mail\\DefaultTemplateResolver",
            'TemplateResolverOpts' => [
                'BasePath' => 'apps/mail_templates',
                'Templates' => [
                    'welcome' => [
                        'Subject' => 'Welcome To',
                        'Path' => 'templates/welcome.html'
                    ]
                ]
            ],

            'LoggerClass' => "RBS\\Communication\\Mail\\DefaultMailLogger",
            'LoggerOpts' => []
        ]
    ],
    'command-packages' => []
];
?>